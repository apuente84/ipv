package com.example.myflix.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myflix.Movie;
import com.example.myflix.R;
import com.example.myflix.RecycleViewAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchFragment extends Fragment implements RecycleViewAdapter.ItemClickListener {

    public static final String TAG = "SearchFragment.TAG";
    public static final String ARG_USER = "ARG_USER";

    Button buttonSearchBack;
    Button buttonSearchMovie;
    EditText editTextSearch;
    ImageView imageViewSearch;
    TextView textViewSearchTitle;
    TextView textViewSearchReleaseDate;
    TextView textViewSearchGenre;
    TextView textViewSearchRating;
    TextView textViewSearchCast;
    TextView textViewSearchPlot;

    private FirebaseAuth mAuth;
    private RecycleViewAdapter adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;

    String currentMovie;

    ArrayList<Movie> searchedMovies = new ArrayList<>();

    public SearchFragment() {
    }

    public static SearchFragment newInstance(String userId) {

        Bundle args = new Bundle();
        args.putString(ARG_USER, userId);

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            final FirebaseFirestore db = FirebaseFirestore.getInstance();
            mAuth = FirebaseAuth.getInstance();

            editTextSearch = getView().findViewById(R.id.editTextSearchMovie);
            imageViewSearch = getView().findViewById(R.id.imageViewSearch);
            textViewSearchTitle = getView().findViewById(R.id.textViewSearchTitle);
            textViewSearchReleaseDate = getView().findViewById(R.id.textViewSearchRelease);
            textViewSearchGenre = getView().findViewById(R.id.textViewSearchGenre);
            //textViewSearchRating = getView().findViewById(R.id.textViewSearchRating);
            //textViewSearchCast = getView().findViewById(R.id.textViewSearchCast);
            textViewSearchPlot = getView().findViewById(R.id.textViewSearchPlot);

            buttonSearchBack = getView().findViewById(R.id.buttonSearchBack);
            buttonSearchBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainFragment mainFragment = MainFragment.newInstance(null);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, mainFragment, MainFragment.TAG)
                            .commit();
                }
            });

            buttonSearchMovie = getView().findViewById(R.id.buttonStartSearch);
            buttonSearchMovie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getSearchData(editTextSearch.getText().toString());
                }
            });

            // Add to Favorites
            ImageButton addToFavoritesButton = getView().findViewById(R.id.buttonSearchAddToFavs);
            // TODO: Save movie to user favorites db
            addToFavoritesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Add to favorites clicked", Toast.LENGTH_SHORT).show();

                    DocumentReference usersFavoritesRef = db.collection("users").document(mAuth.getUid());
                    // Add a new movie to the "favorites" array field.
                    usersFavoritesRef.update("favorites", FieldValue.arrayUnion(currentMovie));
                    // Remove a movie from the "favorites" array field.
                    //usersFavoritesRef.update("favorites", FieldValue.arrayRemove(currentMovie));
                }
            });

            // TODO: Populate movie recycleView based on amount of movies pulled
            recyclerView = getView().findViewById(R.id.recycleViewSearch);
            layoutManager = new LinearLayoutManager(getContext(),
                    LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            adapter = new RecycleViewAdapter(getActivity(), searchedMovies);
            adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);

        }
    }

    public void onItemClick(View view, int position) {
        //TODO: Pull movies poster and update
        imageViewSearch.setImageBitmap(adapter.getItem(position).getMoviePoster());

        textViewSearchTitle.setText(adapter.getItem(position).getTitle());
        textViewSearchReleaseDate.setText(adapter.getItem(position).getReleaseDate());
        textViewSearchGenre.setText(adapter.getItem(position).getGenre());
        //textViewSearchRating.setText(adapter.getItem(position).getRating());
        //textViewSearchCast.setText(adapter.getItem(position).getCast());
        textViewSearchPlot.setText(adapter.getItem(position).getPlot());

        currentMovie = adapter.getItem(position).getTitle() + "#" + adapter.getItem(position).getReleaseDate()
                + "#" + adapter.getItem(position).getGenre() + "#" +
                "#" + "#" + adapter.getItem(position).getPlot() +
                "#" + adapter.getItem(position).getMovieImageUrl() + "#" + adapter.getItem(position).getMovieBackdropUrl();
    }

    public void getSearchData(String movieTitle) {

        searchedMovies.clear();

        //String omdbKey = "63f3a825";
        //String movieSearchUrl = "http://www.omdbapi.com/?apikey=63f3a825&t="; // Movie title at end of request
        //String testUrl = "http://www.omdbapi.com/?apikey=63f3a825&t=scarface";

        //String tmdbSearchTest = "https://api.themoviedb.org/3/search/movie?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&query=scarface&page=1&include_adult=false";

        String tmdbSearchStart = "https://api.themoviedb.org/3/search/movie?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&query=";
        String tmdbSearchEnd = "&page=1&include_adult=false";

        // Request string response from TMDB Search API
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tmdbSearchStart + movieTitle + tmdbSearchEnd,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject outerMostObject = new JSONObject(response);
                            JSONArray results = outerMostObject.getJSONArray("results");

                            for (int i = 0; i < results.length(); i++) {
                                JSONObject movie = results.getJSONObject(i);
                                String title = movie.getString("title");
                                int movieId = movie.getInt("id");
                                // Todo: Some results lack release dates
                                String releaseDate = movie.getString("release_date");
                                JSONArray genres = movie.getJSONArray("genre_ids");
                                // Todo: Some results lack genres
                                if (genres != null) {
                                    String genreId = genres.get(0).toString();
                                }
                                String genre = "Genre not found";
//                                switch (genreId) {
//                                    case "28":
//                                        genre = "Action";
//                                        break;
//                                    case "12":
//                                        genre = "Adventure";
//                                        break;
//                                    case "16":
//                                        genre = "Animation";
//                                        break;
//                                    case "35":
//                                        genre = "Comedy";
//                                        break;
//                                    case "80":
//                                        genre = "Crime";
//                                        break;
//                                    case "99":
//                                        genre = "Documentary";
//                                        break;
//                                    case "18":
//                                        genre = "Drama";
//                                        break;
//                                    case "10751":
//                                        genre = "Family";
//                                        break;
//                                    case "14":
//                                        genre = "Fantasy";
//                                        break;
//                                    case "36":
//                                        genre = "History";
//                                        break;
//                                    case "27":
//                                        genre = "Horror";
//                                        break;
//                                    case "10402":
//                                        genre = "Music";
//                                        break;
//                                    case "9648":
//                                        genre = "Mystery";
//                                        break;
//                                    case "10749":
//                                        genre = "Romance";
//                                        break;
//                                    case "878":
//                                        genre = "Science Fiction";
//                                        break;
//                                    case "10770":
//                                        genre = "TV Movie";
//                                        break;
//                                    case "53":
//                                        genre = "Thriller";
//                                        break;
//                                    case "10752":
//                                        genre = "War";
//                                        break;
//                                    case "37":
//                                        genre = "Western";
//                                        break;
//                                    default:
//                                        genre = "Unknown";
//                                }
                                String plot = movie.getString("overview");
                                String posterPath = movie.getString("poster_path");

                                if (posterPath.isEmpty()) {
                                    posterPath = "/nGY6NnlDsWaRKAycWUgXanqLxia.jpg";
                                }

                                String backdropPath = movie.getString("backdrop_path");

                                if (backdropPath.isEmpty()) {
                                    backdropPath = "/BCIaTLdgNwi3LwknKaI1ys23o7.jpg";
                                }

                                //Bitmap movieImageTest = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.venom);

                                String testRelease = "no release available";

                                Movie newMovie = new Movie(title, "Released: " + testRelease,
                                        "Genre: " + genre, plot, posterPath, backdropPath,
                                        null, null, movieId, null);
                                searchedMovies.add(newMovie);
                            }

                            //Populate first movies data
                            textViewSearchTitle.setText(searchedMovies.get(0).getTitle());
                            textViewSearchReleaseDate.setText(searchedMovies.get(0).getReleaseDate());
                            textViewSearchGenre.setText(searchedMovies.get(0).getGenre());
                            //textViewSearchRating.setText(searchedMovies.get(0).getRating());
                            //textViewSearchCast.setText(searchedMovies.get(0).getCast());
                            textViewSearchPlot.setText(searchedMovies.get(0).getPlot());

                            getMovieImages();
                            //getMoviePosters();
                            //getOmdbData();

                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Try searching again...");
            }
        });

        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(stringRequest);

    }

    public void getMovieImages() {

        String imagePullStartURL = "https://image.tmdb.org/t/p/w500";

        for (int i = 0; i < searchedMovies.size(); i++) {
            String completeImgUrl = imagePullStartURL + searchedMovies.get(i).getMovieImageUrl();
            final int finalI = i;
            ImageRequest request = new ImageRequest(completeImgUrl,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            searchedMovies.get(finalI).setMovieImage(bitmap);
                            adapter.notifyDataSetChanged();
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Something went wrong with getting the image");
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(getContext());
            queue.add(request);
        }

        for (int i = 0; i < searchedMovies.size(); i++) {
            String testUrl = imagePullStartURL + searchedMovies.get(i).getMovieBackdropUrl();
            final int finalI = i;
            ImageRequest request = new ImageRequest(testUrl,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            searchedMovies.get(finalI).setMoviePoster(bitmap);
                            imageViewSearch.setImageBitmap(searchedMovies.get(0).getMoviePoster());
                            adapter.notifyDataSetChanged();
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Something went wrong with getting the image");
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(getContext());
            queue.add(request);
        }

//        String movieSearchUrl = "http://www.omdbapi.com/?apikey=63f3a825&t="; // Movie title at end of request
//
//        for (int mov = 0; mov < moviesData.size(); mov++) {
//
//            // Request string response from OMDB API
//            final int finalMov = mov;
//            StringRequest stringRequest = new StringRequest(Request.Method.GET, movieSearchUrl + moviesData.get(mov).getTitle(),
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject outerMostObject = new JSONObject(response);
//                                String rating = outerMostObject.getString("Title");
//                                String cast = outerMostObject.getString("Actors");
//                                String imdbId = outerMostObject.getString("imdbID");
//                                System.out.println("Rating: " + rating + "\nCast: " + cast);
//
//                                moviesData.get(finalMov).setRating(rating);
//                                moviesData.get(finalMov).setCast(cast);
//                                // Populate first movies data
////                            textViewRating.setText(moviesData.get(0).getRating());
////                            textViewCast.setText(moviesData.get(0).getCast());
//
//                                //getMovieImages();
//                                adapter.notifyDataSetChanged();
//
//                            } catch (JSONException j) {
//                                j.printStackTrace();
//                            }
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    System.out.println("Try again...");
//                }
//            });
//
//            RequestQueue queue = Volley.newRequestQueue(getContext());
//            queue.add(stringRequest);
//
//        }
    }
}
