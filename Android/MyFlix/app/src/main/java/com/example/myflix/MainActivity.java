package com.example.myflix;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.myflix.fragments.LoginFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            LoginFragment fragment = LoginFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment, LoginFragment.TAG)
                    .commit();
        }
    }
}