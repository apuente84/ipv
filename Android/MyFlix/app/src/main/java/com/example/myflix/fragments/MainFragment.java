package com.example.myflix.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myflix.Movie;
import com.example.myflix.R;
import com.example.myflix.RecycleViewAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.protobuf.StringValue;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainFragment extends Fragment implements RecycleViewAdapter.ItemClickListener {

    public static final String TAG = "MainFragment.TAG";
    public static final String ARG_USER = "ARG_USER";

    YouTubePlayerView youTubePlayerView;

    private FirebaseAuth mAuth;
    private RecycleViewAdapter adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;

    ImageButton addToFavoritesButton;

    ScrollView scrollView;

    ImageView imageViewPoster;
    TextView textViewTitle;
    TextView textViewReleaseDate;
    TextView textViewGenre;
    TextView textViewPlot;
    Button buttonNowPlaying;
    Button buttonPopular;
    Button buttonUpcoming;

    ArrayList<Movie> moviesData = new ArrayList<>();

    String currentMovie;
    String currentMovieTrailerPath;

    public MainFragment() {
    }

    public static MainFragment newInstance(String userId) {

        Bundle args = new Bundle();
        args.putString(ARG_USER, userId);

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            scrollView = getView().findViewById(R.id.scrollViewDetails);

            addToFavoritesButton = getView().findViewById(R.id.buttonMainAddToFavs);
            buttonNowPlaying = getView().findViewById(R.id.buttonNowPlaying);
            buttonPopular = getView().findViewById(R.id.buttonPopular);
            buttonUpcoming = getView().findViewById(R.id.buttonUpcoming);

            final FirebaseFirestore db = FirebaseFirestore.getInstance();
            mAuth = FirebaseAuth.getInstance();

            youTubePlayerView = getView().findViewById(R.id.youtube_player_view);
            getLifecycle().addObserver(youTubePlayerView);

            imageViewPoster = getView().findViewById(R.id.imageViewPoster);
            textViewTitle = getView().findViewById(R.id.textViewTitle);
            textViewReleaseDate = getView().findViewById(R.id.textViewReleaseDate);
            textViewGenre = getView().findViewById(R.id.textViewGenre);
            textViewPlot = getView().findViewById(R.id.textViewPlot);

            // Populate main screen on startup
            getMovies("https://api.themoviedb.org/3/movie/now_playing?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1");

            // Search for a movie
            ImageButton searchButton = getView().findViewById(R.id.buttonSearch);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SearchFragment searchFragment = SearchFragment.newInstance(null);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, searchFragment, SearchFragment.TAG)
                            .commit();
                }
            });

            // Settings
            ImageButton settingsButton = getView().findViewById(R.id.buttonSettings);
            // TODO: Transition to settings screen
            settingsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SettingsFragment settingsFragment = SettingsFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .addToBackStack("mainScreen")
                            .replace(R.id.fragment_container, settingsFragment, SettingsFragment.TAG)
                            .commit();
                }
            });

            // Save movie to user favorites db
            addToFavoritesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Added To Favorites...", Toast.LENGTH_SHORT).show();

                    DocumentReference usersFavoritesRef = db.collection("users").document(mAuth.getUid());
                    // Add a new movie to the "favorites" array field.
                    usersFavoritesRef.update("favorites", FieldValue.arrayUnion(currentMovie));
                    // Remove a movie from the "favorites" array field.
                    //usersFavoritesRef.update("favorites", FieldValue.arrayRemove(currentMovie));
                }
            });


            // TODO: Populate movie recycleView based on amount of movies pulled
            recyclerView = getView().findViewById(R.id.recycleView);
            layoutManager = new LinearLayoutManager(getContext(),
                    LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            adapter = new RecycleViewAdapter(getActivity(), moviesData);
            adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);

            // Select Movie Category Buttons
            buttonNowPlaying.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMovies("https://api.themoviedb.org/3/movie/now_playing?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1");
                }
            });

            buttonPopular.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMovies("https://api.themoviedb.org/3/movie/popular?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1");
                }
            });

            buttonUpcoming.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMovies("https://api.themoviedb.org/3/movie/upcoming?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1");
                }
            });

            // To User Favorites
            Button buttonToFavorites = getView().findViewById(R.id.buttonToFavorites);
            buttonToFavorites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FavoritesFragment favoritesFragment = FavoritesFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .addToBackStack("mainScreen")
                            .replace(R.id.fragment_container, favoritesFragment, FavoritesFragment.TAG)
                            .commit();
                }
            });
        }
    }

    public void onItemClick(View view, int position) {
        //TODO: Pull movies poster and update
        imageViewPoster.setImageBitmap(adapter.getItem(position).getMoviePoster());
        textViewTitle.setText(adapter.getItem(position).getTitle());
        textViewReleaseDate.setText(adapter.getItem(position).getReleaseDate());
        textViewGenre.setText(adapter.getItem(position).getGenre());
        textViewPlot.setText(adapter.getItem(position).getPlot());

        currentMovie = adapter.getItem(position).getTitle() + "#" + adapter.getItem(position).getReleaseDate()
                + "#" + adapter.getItem(position).getGenre() + "#" + adapter.getItem(position).getPlot() +
                "#" + adapter.getItem(position).getMovieImageUrl() + "#" + adapter.getItem(position).getMovieBackdropUrl();

        loadTrailerVideo(adapter.getItem(position).getTrailerPath());
    }

    public void getMovies(String url) {

        moviesData.clear();

        // Request string response from TMDB API
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject outerMostObject = new JSONObject(response);
                            JSONArray results = outerMostObject.getJSONArray("results");

                            for (int i = 0; i < results.length(); i++) {
                                JSONObject movie = results.getJSONObject(i);
                                String title = movie.getString("title");
                                int movieId = movie.getInt("id");
                                String releaseDate = movie.getString("release_date");
                                JSONArray genres = movie.getJSONArray("genre_ids");
                                String genreId = genres.get(0).toString();
                                String genre = "Genre not found";
                                switch (genreId) {
                                    case "28":
                                        genre = "Action";
                                        break;
                                    case "12":
                                        genre = "Adventure";
                                        break;
                                    case "16":
                                        genre = "Animation";
                                        break;
                                    case "35":
                                        genre = "Comedy";
                                        break;
                                    case "80":
                                        genre = "Crime";
                                        break;
                                    case "99":
                                        genre = "Documentary";
                                        break;
                                    case "18":
                                        genre = "Drama";
                                        break;
                                    case "10751":
                                        genre = "Family";
                                        break;
                                    case "14":
                                        genre = "Fantasy";
                                        break;
                                    case "36":
                                        genre = "History";
                                        break;
                                    case "27":
                                        genre = "Horror";
                                        break;
                                    case "10402":
                                        genre = "Music";
                                        break;
                                    case "9648":
                                        genre = "Mystery";
                                        break;
                                    case "10749":
                                        genre = "Romance";
                                        break;
                                    case "878":
                                        genre = "Science Fiction";
                                        break;
                                    case "10770":
                                        genre = "TV Movie";
                                        break;
                                    case "53":
                                        genre = "Thriller";
                                        break;
                                    case "10752":
                                        genre = "War";
                                        break;
                                    case "37":
                                        genre = "Western";
                                        break;
                                }
                                String plot = movie.getString("overview");
                                String posterPath = movie.getString("poster_path");

                                if (posterPath.isEmpty()) {
                                    posterPath = "/nGY6NnlDsWaRKAycWUgXanqLxia.jpg";
                                }

                                String backdropPath = movie.getString("backdrop_path");

                                if (backdropPath.isEmpty()) {
                                    backdropPath = "/BCIaTLdgNwi3LwknKaI1ys23o7.jpg";
                                }

                                Movie newMovie = new Movie(title, "Released: " + releaseDate,
                                        "Genre: " + genre, plot, posterPath, backdropPath,
                                        null, null, movieId, null);
                                moviesData.add(newMovie);
                            }

                            // Populate first movies data
                            textViewTitle.setText(moviesData.get(0).getTitle());
                            textViewReleaseDate.setText(moviesData.get(0).getReleaseDate());
                            textViewGenre.setText(moviesData.get(0).getGenre());
                            textViewPlot.setText(moviesData.get(0).getPlot());

                            getMovieImages();
                            getMovieTrailer();

                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Try again...");
            }
        });

        RequestQueue queue = Volley.newRequestQueue(getView().getContext());
        queue.add(stringRequest);
    }

    public void getMovieImages() {

        String imagePullStartURL = "https://image.tmdb.org/t/p/w500";

        for (int i = 0; i < moviesData.size(); i++) {
            String completeImgUrl = imagePullStartURL + moviesData.get(i).getMovieImageUrl();
            final int finalI = i;
            ImageRequest request = new ImageRequest(completeImgUrl,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            moviesData.get(finalI).setMovieImage(bitmap);
                            adapter.notifyDataSetChanged();
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Something went wrong with getting the image");
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(getView().getContext());
            queue.add(request);
        }

        for (int i = 0; i < moviesData.size(); i++) {
            String testUrl = imagePullStartURL + moviesData.get(i).getMovieBackdropUrl();
            final int finalI = i;
            ImageRequest request = new ImageRequest(testUrl,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            moviesData.get(finalI).setMoviePoster(bitmap);
                            imageViewPoster.setImageBitmap(moviesData.get(0).getMoviePoster());
                            adapter.notifyDataSetChanged();
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Something went wrong with getting the image");
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(getView().getContext());
            queue.add(request);
        }
    }

    private void getMovieTrailer() {

        String trailerPathStart = "https://api.themoviedb.org/3/movie/";
        String trailerPathEnd = "/videos?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US";

        for (int i = 0; i < moviesData.size(); i++) {

            String trailerPathComplete = trailerPathStart + moviesData.get(i).getMovieId() + trailerPathEnd;

            final int finalI = i;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, trailerPathComplete,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject outerMostObject = new JSONObject(response);
                                JSONArray results = outerMostObject.getJSONArray("results");
                                String trailerPath = results.getJSONObject(0).getString("key");
                                moviesData.get(finalI).setTrailerPath(trailerPath);

                            } catch (JSONException j) {
                                j.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("Could not get video path");
                }
            });

            RequestQueue queue = Volley.newRequestQueue(getContext());
            queue.add(stringRequest);
        }
        loadTrailerVideo(moviesData.get(0).getTrailerPath());
    }

//    private String getMovieTrailer(String movieId) {
//
//        final String[] trailerPath = {""};
//        String trailerPathStart = "https://api.themoviedb.org/3/movie/";
//        String trailerPathEnd = "/videos?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US";
//        String trailerPathComplete = trailerPathStart + movieId + trailerPathEnd;
//
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, trailerPathComplete,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject outerMostObject = new JSONObject(response);
//                            JSONArray results = outerMostObject.getJSONArray("results");
//                            trailerPath[0] = results.getJSONObject(0).getString("key");
//
//                        } catch (JSONException j) {
//                            j.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                System.out.println("Could not get video path");
//            }
//        });
//
//        RequestQueue queue = Volley.newRequestQueue(getContext());
//        queue.add(stringRequest);
//
//        return trailerPath[0];
//
//    }

    public void loadTrailerVideo(final String currentMovieTrailerPath) {
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId = "WNNUcHRiPS8";

                if (currentMovieTrailerPath == null) {
                    youTubePlayer.cueVideo(videoId, 0f);
                } else {
                    youTubePlayer.cueVideo(currentMovieTrailerPath, 0f);
                }
            }
        });

    }
}
