package com.example.myflix.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myflix.R;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;

public class SettingsFragment extends Fragment {

    public static final String TAG = "SettingsFragment.TAG";
    private FirebaseAuth mAuth;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            mAuth = FirebaseAuth.getInstance();

            final EditText editTextEmail = getView().findViewById(R.id.editTextEmailUpdate);
            final EditText editTextPassword = getView().findViewById(R.id.editTextPasswordUpdate);

            editTextEmail.setHint(mAuth.getCurrentUser().getEmail());
            editTextPassword.setHint("**********");

            Button updateButton = getView().findViewById(R.id.buttonUpdate);
            // TODO: Setup transition to Main Screen
            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editTextEmail.getText().toString().isEmpty() || editTextPassword.getText().toString().isEmpty()) {
                        Toast.makeText(getContext(), "Please do not leave anything blank. Select Cancel to return to the main menu", Toast.LENGTH_SHORT).show();
                    } else {

                        mAuth.getCurrentUser().updateEmail(editTextEmail.getText().toString());
                        mAuth.getCurrentUser().updatePassword(editTextPassword.getText().toString());

                        Toast.makeText(getContext(), "Credentials Updated", Toast.LENGTH_SHORT).show();

                        getFragmentManager().popBackStack();
//                        MainFragment mainFragment = MainFragment.newInstance(mAuth.getUid());
//                        getFragmentManager().beginTransaction()
//                                .replace(R.id.fragment_container, mainFragment, MainFragment.TAG)
//                                .commit();
                    }
                }
            });

            Button cancelButton = getView().findViewById(R.id.buttonCancelUpdate);
            // TODO: Setup transition to Password Recovery Screen
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFragmentManager().popBackStack();
//                    MainFragment mainFragment = MainFragment.newInstance(null);
//                    getFragmentManager().beginTransaction()
//                            .replace(R.id.fragment_container, mainFragment, MainFragment.TAG)
//                            .commit();
                }
            });

            // Logout
            Button logoutButton = getView().findViewById(R.id.buttonLogout);
            logoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAuth.signOut();

                    //LoginManager.getInstance().logOut();

                    LoginFragment loginFragment = LoginFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, loginFragment, LoginFragment.TAG)
                            .commit();
                }
            });
        }
    }
}
