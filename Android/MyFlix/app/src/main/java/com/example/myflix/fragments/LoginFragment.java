package com.example.myflix.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myflix.MainActivity;
import com.example.myflix.Movie;
import com.example.myflix.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


public class LoginFragment extends Fragment {

    public static final String TAG = "LoginFragment.TAG";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    TextView emailError;
    TextView passwordError;

    CallbackManager mCallbackManager;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            db = FirebaseFirestore.getInstance();
            mAuth = FirebaseAuth.getInstance();

            final EditText editTextEmail = getView().findViewById(R.id.editTextEmail);
            final EditText editTextPassword = getView().findViewById(R.id.editTextPassword);
            emailError = getView().findViewById(R.id.textViewEmailError);
            emailError.setAlpha(0);
            passwordError = getView().findViewById(R.id.textViewPasswordError);
            passwordError.setAlpha(0);

            editTextEmail.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!isValidEmail(editTextEmail.getText().toString())) {
                        emailError.setAlpha(1);
                        emailError.setText("Please enter a valid email address...");
                    } else {
                        //emailError.setText("Looking Good...");
                        emailError.setAlpha(0);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            editTextPassword.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!isValidPassword(editTextPassword.getText().toString())) {
                        passwordError.setText("Please enter valid password...");
                        passwordError.setAlpha(1);
                    } else {
                        //emailError.setText("Looking Good...");
                        passwordError.setAlpha(0);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            // Login and transition to main screen
            final Button loginButton = getView().findViewById(R.id.buttonLogin);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editTextEmail.getText().toString().isEmpty() || editTextPassword.getText().toString().isEmpty()) {
                        Toast.makeText(getContext(), "Please Check Login Credentials...", Toast.LENGTH_SHORT).show();
                    } else {

                        mAuth.signInWithEmailAndPassword(editTextEmail.getText().toString(), editTextPassword.getText().toString())
                                .addOnCompleteListener(Objects.requireNonNull(getActivity()), new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in successful
                                            Log.d(TAG, "Sign in successful");
                                            FirebaseUser user = mAuth.getCurrentUser();

                                            MainFragment fragment = MainFragment.newInstance(Objects.requireNonNull(user).getUid());
                                            Objects.requireNonNull(getFragmentManager()).beginTransaction()
                                                    .addToBackStack("mainScreen")
                                                    .replace(R.id.fragment_container, fragment, MainFragment.TAG)
                                                    .commit();

                                        } else {
                                            // Sign in failed
                                            Log.w(TAG, "Sign in Failed", task.getException());
                                            Toast.makeText(getActivity(), "Login Failed...Please Try Again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
            });

            // Initialize Facebook Login button
            mCallbackManager = CallbackManager.Factory.create();
            LoginButton fbLoginButton = getView().findViewById(R.id.facebook_login_button);
            fbLoginButton.setReadPermissions("email", "public_profile");
            fbLoginButton.setFragment(LoginFragment.this);
            fbLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d(TAG, "facebook:onSuccess:" + loginResult);
                    handleFacebookAccessToken(loginResult.getAccessToken());
                    //System.out.println("This worked!!!");

                    Map<String, Object> user = new HashMap<>();
                    user.put("email", mAuth.getCurrentUser().getEmail());

                    // Add new document to database using userId as document ID
                    db.collection("users").document(Objects.requireNonNull(mAuth.getUid())).set(user)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "Document added with ID: " + mAuth.getUid());
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });

                    // Successful registration, proceed to main screen
                            MainFragment mainFragment = MainFragment.newInstance(mAuth.getUid());
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, mainFragment, MainFragment.TAG)
                                    .commit();
                }

                @Override
                public void onCancel() {
                    Log.d(TAG, "facebook:onCancel");
                    // ...
                }

                @Override
                public void onError(FacebookException error) {
                    Log.d(TAG, "facebook:onError", error);
                    // ...
                }
            });

            Button forgotPasswordButton = getView().findViewById(R.id.buttonForgotPassword);
            // TODO: Setup transition to Password Recovery Screen
            forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PassRecovFragment passRecovFragment = PassRecovFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, passRecovFragment, PassRecovFragment.TAG)
                            .commit();
                }
            });

            Button newAccountButton = getView().findViewById(R.id.buttonNewAccount);
            // TODO: Setup transition to New Account Screen
            newAccountButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RegisterFragment registerFragment = RegisterFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, registerFragment, RegisterFragment.TAG)
                            .commit();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public static boolean isValidEmail(CharSequence emailAddress) {
        return (!TextUtils.isEmpty(emailAddress) && Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches());
    }

    // Requires 1 Uppercase, 1 Number, and 1 Symbol
    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            System.out.println(user + " is the current user!!!");

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
