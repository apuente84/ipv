package com.example.myflix.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myflix.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class PassRecovFragment extends Fragment {

    public static final String TAG = "PassRecovFragment.TAG";

    private FirebaseAuth mAuth;

    public static PassRecovFragment newInstance() {
        return new PassRecovFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.passrecov_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            mAuth = FirebaseAuth.getInstance();

            final EditText editTextRecoveryEmail = getView().findViewById(R.id.editTextEmailRecover);

            Button recoverButton = getView().findViewById(R.id.buttonRecover);
            // TODO: Setup register new user and transition to Main Screen
            recoverButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mAuth.sendPasswordResetEmail(editTextRecoveryEmail.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getContext(), "Check your email...", Toast.LENGTH_SHORT).show();
                                        LoginFragment loginFragment = LoginFragment.newInstance();
                                        getFragmentManager().beginTransaction()
                                                .replace(R.id.fragment_container, loginFragment, LoginFragment.TAG)
                                                .commit();
                                    }
                                }
                            });
                }
            });


            Button cancelButton = getView().findViewById(R.id.buttonCancelRecovery);
            // TODO: Setup transition back to login screen
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginFragment loginFragment = LoginFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, loginFragment, LoginFragment.TAG)
                            .commit();
                }
            });
        }
    }
}
