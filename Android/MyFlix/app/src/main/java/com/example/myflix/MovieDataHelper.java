package com.example.myflix;

import android.os.FileUtils;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class MovieDataHelper {

    //private String key = "5d3e508cd0191203f68265bfcd599e14";

    public static String getNowPlayingData() {

        try {
            URL url = new URL("https://api.themoviedb.org/3/movie/now_playing?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1");
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            String data = IOUtils.toString(inputStream, String.valueOf(StandardCharsets.UTF_8));
            inputStream.close();
            connection.disconnect();
            return data;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Movie parseMovie(String data) {

        try {
            JSONObject outerMostObject = new JSONObject(data);

            System.out.println(outerMostObject);
        } catch (JSONException j) {
            j.printStackTrace();
        }
        return null;
    }

}
