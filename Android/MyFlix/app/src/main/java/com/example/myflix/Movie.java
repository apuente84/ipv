package com.example.myflix;

import android.graphics.Bitmap;
import android.os.Bundle;

import java.io.Serializable;

public class Movie implements Serializable {

    private String title;
    private String releaseDate;
    private String genre;
    private String plot;
    private String movieImageUrl;
    private String movieBackdropUrl;
    private Bitmap movieImage;
    private Bitmap moviePoster;
    private int movieId;
    private String trailerPath;

    public Movie(String title, String releaseDate, String genre,
                 String plot, String movieImageUrl, String movieBackdropUrl, Bitmap movieImage,
                 Bitmap moviePoster, int movieId, String trailerPath) {
        this.title = title;
        this.releaseDate = releaseDate;
        this.genre = genre;
        this.plot = plot;
        this.movieImageUrl = movieImageUrl;
        this.movieBackdropUrl = movieBackdropUrl;
        this.movieImage = movieImage;
        this.moviePoster = moviePoster;
        this.movieId = movieId;
        this.trailerPath = trailerPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getMovieImageUrl() {
        return movieImageUrl;
    }

    public void setMovieImageUrl(String movieImageUrl) {
        this.movieImageUrl = movieImageUrl;
    }

    public String getMovieBackdropUrl() {
        return movieBackdropUrl;
    }

    public void setMovieBackdropUrl(String movieBackdropUrl) {
        this.movieBackdropUrl = movieBackdropUrl;
    }

    public Bitmap getMovieImage() {
        return movieImage;
    }

    public void setMovieImage(Bitmap movieImage) {
        this.movieImage = movieImage;
    }

    public Bitmap getMoviePoster() {
        return moviePoster;
    }

    public void setMoviePoster(Bitmap moviePoster) {
        this.moviePoster = moviePoster;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getTrailerPath() {
        return trailerPath;
    }

    public void setTrailerPath(String trailerPath) {
        this.trailerPath = trailerPath;
    }
}
