package com.example.myflix.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myflix.Movie;
import com.example.myflix.R;
import com.example.myflix.RecycleViewAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFragment extends Fragment implements RecycleViewAdapter.ItemClickListener {

    public static final String TAG = "FavoritesFragment.TAG";

    private FirebaseAuth mAuth;
    private RecycleViewAdapter adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;

    Button buttonFavoriteBack;
    ImageView imageViewFavorite;
    ImageButton buttonRemoveFavorite;
    TextView textViewFavoriteTitle;
    TextView textViewFavoriteReleaseDate;
    TextView textViewFavoriteGenre;
    TextView textViewFavoritePlot;

    ArrayList<Movie> favoriteMovies = new ArrayList<>();
    ArrayList<String> movieStrings = new ArrayList<>();

    int currentMoviePosition;

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.favorites_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            imageViewFavorite = getView().findViewById(R.id.imageViewFavorite);
            textViewFavoriteTitle = getView().findViewById(R.id.textViewFavoriteTitle);
            textViewFavoriteReleaseDate = getView().findViewById(R.id.textViewFavoriteRelease);
            textViewFavoriteGenre = getView().findViewById(R.id.textViewFavoriteGenre);
            textViewFavoritePlot = getView().findViewById(R.id.textViewFavoritePlot);

            final FirebaseFirestore db = FirebaseFirestore.getInstance();
            mAuth = FirebaseAuth.getInstance();

            buttonFavoriteBack = getView().findViewById(R.id.buttonFavoriteBack);
            // TODO: Setup transition to Password Recovery Screen
            buttonFavoriteBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFragmentManager().popBackStack();
                }
            });

            buttonRemoveFavorite = getView().findViewById(R.id.buttonRemoveFavorite);
            buttonRemoveFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (favoriteMovies.size() > 0) {

                        DocumentReference usersFavoritesRef = db.collection("users").document(mAuth.getUid());
                        // Remove a movie from the "favorites" array field.
                        usersFavoritesRef.update("favorites", FieldValue.arrayRemove(movieStrings.get(currentMoviePosition)));

                        favoriteMovies.remove(currentMoviePosition);
                        adapter.notifyDataSetChanged();

                        if (!favoriteMovies.isEmpty()) {
                            // Populate data for first movie on start
                            imageViewFavorite.setImageBitmap(adapter.getItem(0).getMoviePoster());
                            textViewFavoriteTitle.setText(adapter.getItem(0).getTitle());
                            textViewFavoriteReleaseDate.setText(adapter.getItem(0).getReleaseDate());
                            textViewFavoriteGenre.setText(adapter.getItem(0).getGenre());
                            textViewFavoritePlot.setText(adapter.getItem(0).getPlot());
                        } else {
                            // Populate data for first movie on start
                            imageViewFavorite.setImageBitmap(null);
                            textViewFavoriteTitle.setText("No Favorites Added...");
                            textViewFavoriteReleaseDate.setText("");
                            textViewFavoriteGenre.setText("");
                            textViewFavoritePlot.setText("");
                        }
                    }
                }
            });

            // TODO: Populate movie recycleView based on amount of movies pulled
            recyclerView = getView().findViewById(R.id.recycleViewFavorites);
            layoutManager = new LinearLayoutManager(getContext(),
                    LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            adapter = new RecycleViewAdapter(getActivity(), favoriteMovies);
            adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);


            DocumentReference usersFavorites = db.collection("users").document(mAuth.getUid());
            usersFavorites.get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            DocumentSnapshot document = task.getResult();
                            movieStrings = (ArrayList<String>) document.get("favorites");

                            if (!movieStrings.isEmpty()) {

                                for (int i = 0; i < movieStrings.size(); i++) {
                                    getMovieFromDb(movieStrings.get(i));
                                    adapter.notifyDataSetChanged();
                                }

                                getMovieImages();

                                // Populate data for first movie on start
                                imageViewFavorite.setImageBitmap(adapter.getItem(0).getMoviePoster());
                                textViewFavoriteTitle.setText(adapter.getItem(0).getTitle());
                                textViewFavoriteReleaseDate.setText(adapter.getItem(0).getReleaseDate());
                                textViewFavoriteGenre.setText(adapter.getItem(0).getGenre());
                                textViewFavoritePlot.setText(adapter.getItem(0).getPlot());

                            }
                        }
                    });
        }
    }

    public void onItemClick(View view, int position) {
        //TODO: Pull movies poster and update
        imageViewFavorite.setImageBitmap(adapter.getItem(position).getMoviePoster());

        textViewFavoriteTitle.setText(adapter.getItem(position).getTitle());
        textViewFavoriteReleaseDate.setText(adapter.getItem(position).getReleaseDate());
        textViewFavoriteGenre.setText(adapter.getItem(position).getGenre());
        textViewFavoritePlot.setText(adapter.getItem(position).getPlot());

        currentMoviePosition = position;
    }

    public void getMovieImages() {

        String imagePullStartURL = "https://image.tmdb.org/t/p/w500";

        for (int i = 0; i < favoriteMovies.size(); i++) {
            String completeImgUrl = imagePullStartURL + favoriteMovies.get(i).getMovieImageUrl();
            final int finalI = i;
            ImageRequest request = new ImageRequest(completeImgUrl,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            favoriteMovies.get(finalI).setMovieImage(bitmap);
                            adapter.notifyDataSetChanged();
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Something went wrong with getting the image");
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(getContext());
            queue.add(request);
        }

        for (int i = 0; i < favoriteMovies.size(); i++) {
            String testUrl = imagePullStartURL + favoriteMovies.get(i).getMovieBackdropUrl();
            final int finalI = i;
            ImageRequest request = new ImageRequest(testUrl,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            favoriteMovies.get(finalI).setMoviePoster(bitmap);
                            imageViewFavorite.setImageBitmap(favoriteMovies.get(0).getMoviePoster());
                            adapter.notifyDataSetChanged();
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Something went wrong with getting the image");
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(getContext());
            queue.add(request);
        }
    }

    public void getMovieFromDb(String movieData) {
        String[] separatedString = movieData.split("#");
        Movie pulledMovie = new Movie(separatedString[0], separatedString[1], separatedString[2], separatedString[3], separatedString[4], separatedString[5], null, null, 0, null);

        favoriteMovies.add(pulledMovie);
    }

}
