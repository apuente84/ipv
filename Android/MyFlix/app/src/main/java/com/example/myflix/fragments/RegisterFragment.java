package com.example.myflix.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myflix.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterFragment extends Fragment {

    public static final String TAG = "RegisterFragment.TAG";

    private FirebaseAuth mAuth;

    TextView emailError;
    TextView passwordError;
    Button registerButton;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getView() != null) {

            mAuth = FirebaseAuth.getInstance();
            final FirebaseFirestore db = FirebaseFirestore.getInstance();

            final Button registerButton = getView().findViewById(R.id.buttonRegister);
            registerButton.setClickable(false);
            final EditText editTextNewEmail = getView().findViewById(R.id.editTextNewEmail);
            final EditText editTextNewPassword = getView().findViewById(R.id.editTextNewPassword);
            emailError = getView().findViewById(R.id.textViewRegisterEmailError);
            emailError.setAlpha(0);
            passwordError = getView().findViewById(R.id.textViewRegisterPasswordError);
            //passwordError.setAlpha(0);

            editTextNewEmail.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!isValidEmail(editTextNewEmail.getText().toString())) {
                        emailError.setAlpha(1);
                        emailError.setText("Please enter a valid email address...");
                    } else {
                        emailError.setAlpha(0);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            editTextNewPassword.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!isValidPassword(editTextNewPassword.getText().toString())) {
                        passwordError.setAlpha(1);
                    } else {
                        passwordError.setAlpha(0);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            // TODO: Setup register new user and transition to Main Screen
            registerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Check for valid email format and password (minimum 8 characters with special requirements)
                    if (!isValidEmail(editTextNewEmail.getText().toString()) ||
                            editTextNewPassword.getText().toString().length()<8&&!isValidPassword(editTextNewPassword.getText().toString())) {
                        Toast.makeText(getActivity(), "Please enter valid credentials...", Toast.LENGTH_SHORT).show();
                    } else {

                        mAuth.createUserWithEmailAndPassword(editTextNewEmail.getText().toString(),
                                editTextNewPassword.getText().toString())
                                .addOnCompleteListener(Objects.requireNonNull(getActivity()), new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // User Created
                                            Log.d(TAG, "Created User Successfully");

                                            // Create a new user with email and password
                                            Map<String, Object> user = new HashMap<>();
                                            user.put("email", editTextNewEmail.getText().toString());
                                            user.put("password", editTextNewPassword.getText().toString());

                                            // Add new document to database using userId as document ID
                                            db.collection("users").document(Objects.requireNonNull(mAuth.getUid())).set(user)
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Log.d(TAG, "Document added with ID: " + mAuth.getUid());
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.w(TAG, "Error adding document", e);
                                                }
                                            });

                                            // Successful registration, proceed to main screen
                                            MainFragment mainFragment = MainFragment.newInstance(mAuth.getUid());
                                            getFragmentManager().beginTransaction()
                                                    .replace(R.id.fragment_container, mainFragment, MainFragment.TAG)
                                                    .commit();
                                        } else {
                                            Log.w(TAG, "User not created", task.getException());
                                            Toast.makeText(getActivity(),"User not created", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
            });

            Button cancelButton = getView().findViewById(R.id.buttonCancelRegister);
            // TODO: Setup transition back to login screen
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginFragment loginFragment = LoginFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, loginFragment, LoginFragment.TAG)
                            .commit();
                }
            });

        }
    }

    public static boolean isValidEmail(CharSequence emailAddress) {
        return (!TextUtils.isEmpty(emailAddress) && Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches());
    }

    // Requires 1 Uppercase, 1 Number, and 1 Symbol
    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
