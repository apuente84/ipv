//
//  MainVC_EXT.swift
//  MyFlix
//
//  Created by Tony Puente on 10/27/20.
//

import Foundation
import UIKit

extension MainViewController {
    
    // Get movie data from TMDB
    func getMovieData(jsonAtUrl urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        if let validURL = URL(string: urlString) {
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail on error
                if opt_error != nil { return }
                
                //Check response, statusCode and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize the data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        //Parse Data
                        guard let sourceData = json["results"] as? [[String: Any]]
                            else { assertionFailure(); return
                        }
                        
                        //print(sourceData)
                        
                        //Looping through children and appending
                        for sources in sourceData {
                            guard let mTitle = sources["title"] as? String,
                                let mReleaseDate = sources["release_date"] as? String,
                                let mPlot = sources["overview"] as? String,
                                let mKey = sources["id"] as? Int,
                                let mGenre = sources["genre_ids"] as? [Int],
                                let mPoster = sources["poster_path"] as? String,
                                let mBackdrop = sources["backdrop_path"] as? String
                                else {continue}
                            
                            //var tempGen = 0
                            
                            let newMovie = try Movie(title: mTitle, releaseDate: mReleaseDate, plot: mPlot, movieKey: mKey, genreId: mGenre, poster: mPoster, backdrop: mBackdrop)
                            
                            if urlString == "http://api.themoviedb.org/3/movie/now_playing?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1" {
                                self.nowPlayingTemp.append(newMovie)
                                self.movies.append(newMovie)
                            }
                            else if urlString == "http://api.themoviedb.org/3/movie/top_rated?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1" {
                                self.popularTemp.append(newMovie)
                            }
                            else {
                                self.upcomingTemp.append(newMovie)
                            }
                            
                        }
                        
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
                DispatchQueue.main.async {
                    self.moviesCollectionView.reloadData()
                }
                
            })
            task.resume()
        }
    }
    
    func getMovieTrailer(jsonAtUrl movieId: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let urlStart = "https://api.themoviedb.org/3/movie/"
        let urlEnd = "/videos?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US"
        let completeURL = urlStart + movieId + urlEnd
        
        if let validURL = URL(string: completeURL) {
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail on error
                if opt_error != nil { return }
                
                //Check response, statusCode and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize the data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        //Parse Data
                        guard let sourceData = json["results"] as? [[String: Any]]
                            else { assertionFailure(); return
                        }
                        
                        //print(sourceData)
                        
                        for sources in sourceData {
                            let key = sources["key"] as? String
                            print(key! + " is the currently selected movies trailer key...")
                            self.currentMoviePath = key!
                        }
                        
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
                DispatchQueue.main.async {
                    //self.trailerView.load(withVideoId: self.currentMoviePath, playerVars: ["playsinline": 1])
                    self.trailerView.load(withVideoId: self.currentMoviePath)
                }
                
            })
            task.resume()
        }
    }
    
}
