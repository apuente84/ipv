//
//  SettingsViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 11/22/20.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var settingsImage: UIImageView!
    
    @IBOutlet weak var updateEmailTextField: UITextField!
    @IBOutlet weak var updatePasswordTextField: UITextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateEmailTextField.delegate = self
        updatePasswordTextField.delegate = self
        
        let userEmail = Auth.auth().currentUser?.email
        updateEmailTextField.text = userEmail
        updatePasswordTextField.text = "********"
    }
    
    
    @IBAction func updateTapped(_ sender: UIButton) {
        
        //let userEmail = updateEmailTextField.text
        let userPassword = updatePasswordTextField.text
        
        if userPassword != "********" && validatePassword(userPassword!){
            
            Auth.auth().currentUser?.updatePassword(to: userPassword!, completion: { (error) in
                if error != nil {
                    
                    print(error!.localizedDescription)
                    
                } else {
                    print("Password Update Successfull")
                    self.performSegue(withIdentifier: "unwindToMain", sender: self)
                }
            })
            
        }
        
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        // Set up Unwind feature
        self.performSegue(withIdentifier: "unwindToMain", sender: self)
    }
    
    // Check for valid password
    func validatePassword(_ password: String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }

    @IBAction func logoutTapped(_ sender: UIButton) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
