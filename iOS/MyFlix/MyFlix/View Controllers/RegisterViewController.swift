//
//  RegisterViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 10/18/20.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var newEmailTextField: UITextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        newEmailTextField.delegate = self
        newPasswordTextField.delegate = self
        
        // Hide error labels
        emailErrorLabel.alpha = 0
        passwordErrorLabel.alpha = 0
        errorLabel.alpha = 0
    }
    
    
    // MARK: - Actions
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        let error = validateUserInput()
        
        if error != nil {
            // Display error message
            displayError(error!)
        } else {
            // Trim Data
            let email = newEmailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = newPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            // Create new user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                
                //Check for error
                if err != nil {
                    // Error creating user
                    self.displayError("Error - User not created.")
                }
                else {
                    // User created, create DB fields
                    let db = Firestore.firestore()
                    db.collection("users").document(result!.user.uid).setData(["email":email, "password":password, "favorites": []]) { (error) in
                        
                        if error != nil {
                            //Display error
                            self.displayError("Error - user data not saved.")
                        }
                    }
                }
                // To MainVC
                self.transitionToMain()
            }
        }
    }
    
    // MARK: - Methods
    
    func validateUserInput() -> String? {
        
        // Verify all text fields are filled in
        if newEmailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                return "Please fill in all fields..."
        }
        
        // Check secure password
        let password = newPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if validatePassword(password) == false {
            // Password does not meet security format
            return "Ensure password contains at least 8 characters, 1 special character, and 1 number."
        }
        return nil
    }
    
    
    @IBAction func newEmailErrorAlert(_ sender: UITextField) {
        let newEmail = newEmailTextField.text
        
        if !isValidEmail(newEmail!) {
            emailErrorLabel.alpha = 1
            emailErrorLabel.text = "Enter valid email..."
        } else {
            emailErrorLabel.alpha = 0
        }
    }
    
    @IBAction func newPasswordErrorAlert(_ sender: UITextField) {
        let newPassword = newPasswordTextField.text
        
        if !validatePassword(newPassword!) {
            passwordErrorLabel.alpha = 1
            passwordErrorLabel.text = "Password must contain 8 characters, 1 Uppercase, 1 Number, 1 Symbol"
        } else {
            passwordErrorLabel.alpha = 0
        }
    }
    
    
    // Check for valid email
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Check for valid password
    func validatePassword(_ password: String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    // Show error message
    func displayError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Go to next text field
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    // MARK: - Navigation
    
    // Transition to main view controller
    func transitionToMain () {
        let mainViewController = storyboard?.instantiateViewController(withIdentifier: "MainVC") as? MainViewController
        view.window?.rootViewController = mainViewController
        view.window?.makeKeyAndVisible()
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
