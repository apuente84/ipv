//
//  SearchViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 11/10/20.
//

import UIKit
import youtube_ios_player_helper

class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var searchTitleTextField: UITextField!
    
    @IBOutlet weak var searchDetailScrollView: UIView!
    @IBOutlet weak var searchMoviesCollectionView: UICollectionView!
    
    @IBOutlet weak var searchMoviePosterImage: UIImageView!
    @IBOutlet weak var searchMovieTitleLabel: UILabel!
    @IBOutlet weak var searchRuntimeGenreLabel: UILabel!
    @IBOutlet weak var searchReleaseDateLabel: UILabel!
    @IBOutlet weak var searchPlotLabel: UILabel!
    
    @IBOutlet weak var trailerView: YTPlayerView!
    
    var movies = [Movie]()
    var searchedMovies = [Movie]()
    
    var currentMovieId = 0
    var currentMoviePath = ""
    var currentMoviePosition = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchTitleTextField.delegate = self
    }
    
    // MARK: Methods
    
    @IBAction func searchTapped(_ sender: UIButton) {
        
        searchedMovies.removeAll()
        
        performSearch()
        
        searchTitleTextField.text = ""
        
        searchMoviePosterImage.image = nil
    }
    
    @IBAction func backTapped(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "unwindToMain", sender: self)
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {

        //textField code

        textField.resignFirstResponder()  //if desired
        performSearch()
        return true
    }
    
    // MARK: CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as!
        MovieCollectionViewCell
        
        cell.movieImage.image = movies[indexPath.row].movieImage
        
        if searchMoviePosterImage.image == nil{
            // Display first movie data on startup
            searchMoviePosterImage.image = movies[0].movieBackground
            searchMovieTitleLabel.text = movies[0].title
            searchRuntimeGenreLabel.text = "Genre: \(movies[0].genres)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let newDateFormatter = DateFormatter()
            newDateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = dateFormatter.date(from: movies[0].releaseDate) {
                searchReleaseDateLabel.text = "Released: \(newDateFormatter.string(from: date))"
            }
            else {
                searchReleaseDateLabel.text = "Released: Unknown"
            }
            searchPlotLabel.text = movies[0].plot
            
            getMovieTrailer(jsonAtUrl: String(movies[0].movieKey))
            
            // Show all labels
            searchMovieTitleLabel.alpha = 1
            searchRuntimeGenreLabel.alpha = 1
            searchReleaseDateLabel.alpha = 1
            searchPlotLabel.alpha = 1
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Showing all labels
        searchMovieTitleLabel.textColor = UIColor.white
        searchMovieTitleLabel.alpha = 1
        searchRuntimeGenreLabel.alpha = 1
        searchReleaseDateLabel.alpha = 1
        searchPlotLabel.alpha = 1
        
        // Reset scrollView to top
        //detailScrollView.setContentOffset(CGPoint(x: 0.0,y: 0.0), animated: false)
        
        // Reset movie list to beginning when movie category is changed, this doenst belong here, movie it when needed!!!
        //collectionView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: false)
        
        searchMoviePosterImage.image = movies[indexPath.row].movieBackground
        searchMovieTitleLabel.text = movies[indexPath.row].title
        searchRuntimeGenreLabel.text = "Genre: \(movies[indexPath.row].genres)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "MMM dd, yyyy"
        
        if let date = dateFormatter.date(from: movies[indexPath.row].releaseDate) {
            searchReleaseDateLabel.text = "Released: \(newDateFormatter.string(from: date))"
        }
        else {
            searchReleaseDateLabel.text = "Release: Unknown"
        }
        
        searchPlotLabel.text = movies[indexPath.row].plot
        
        getMovieTrailer(jsonAtUrl: String(movies[indexPath.row].movieKey))
        
        // Set currentMovieId
        currentMovieId = movies[indexPath.row].movieKey    }
    
    
    func performSearch() {
        
        let urlStart = "https://api.themoviedb.org/3/search/movie?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&query="
        let urlEnd = "&page=1&include_adult=false"
        let movieKeyword = searchTitleTextField.text
        
        movies.removeAll()
        getMovieData(jsonAtUrl: urlStart + movieKeyword! + urlEnd)
        movies = searchedMovies
        searchMoviesCollectionView.reloadData()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
