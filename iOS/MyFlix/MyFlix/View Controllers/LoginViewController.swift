//
//  LoginViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 10/18/20.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var newAccountButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let loginButton = FBLoginButton()
//        loginButton.center = view.center
//                view.addSubview(loginButton)
        
        // Do any additional setup after loading the view.
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        // Hide error labels
        emailError.alpha = 0
        passwordError.alpha = 0
        errorLabel.alpha = 0
    }
    
    // MARK: - Actions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let error = validateUserInput()
        if error != nil {
            displayError(error!)
        } else {
            let emailAddress = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            // Sign user in via Firebase Auth
            Auth.auth().signIn(withEmail: emailAddress, password: password) { (result, error) in
                if error != nil {
                    // Sign in failed
                    self.errorLabel.text = error!.localizedDescription
                    self.errorLabel.alpha = 1
                } else {
                    self.transitionToMain()
                }
            }
        }
    }
    
    // MARK: - Methods
    func validateUserInput() -> String? {
        // Verify all text fields are filled in
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields..."
        }
        return nil
    }
    
    // Show error message
    func displayError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    @IBAction func emailErrorAlert(_ sender: UITextField) {
        let email = emailTextField.text
        
        if !isValidEmail(email!) {
            emailError.alpha = 1
            emailError.text = "Please enter a valid email..."
        } else {
            emailError.alpha = 0
        }
    }
    
    @IBAction func passwordErrorAlert(_ sender: UITextField) {
        let password = passwordTextField.text
        
        if !validatePassword(password!) {
            passwordError.alpha = 1
            passwordError.text = "Please enter valid password..."
        } else {
            passwordError.alpha = 0
        }
    }
    
    // Check for valid email
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Check for valid password
    func validatePassword(_ password: String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Navigation
    
    // Transition to main view controller
    func transitionToMain () {
        let mainViewController = storyboard?.instantiateViewController(withIdentifier: "MainVC") as? MainViewController
        view.window?.rootViewController = mainViewController
        view.window?.makeKeyAndVisible()
    }
    
    @IBAction func unwindToLogin(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
