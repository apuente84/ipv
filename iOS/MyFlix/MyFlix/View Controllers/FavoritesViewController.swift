//
//  FavoritesViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 11/10/20.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import youtube_ios_player_helper

class FavoritesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var detailScrollView: UIView!
    @IBOutlet weak var favMoviesCollectionView: UICollectionView!
    
    @IBOutlet weak var favMoviePosterImage: UIImageView!
    @IBOutlet weak var favMovieTitleLabel: UILabel!
    @IBOutlet weak var favRuntimeGenreLabel: UILabel!
    @IBOutlet weak var favReleaseDateLabel: UILabel!
    @IBOutlet weak var favPlotLabel: UILabel!

    @IBOutlet weak var trailerView: YTPlayerView!
    
    var movies = [Movie]()
    var favoriteMovies = [Movie]()
    var currentMoviePath = ""
    var currentMovieId = 0
    var movieStrings = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let userId = Auth.auth().currentUser?.uid
        let db = Firestore.firestore()
        
        let docRef = db.collection("users").document(userId!)
        
        docRef.getDocument { document, error in
            
            guard error == nil, let document = document, document.exists, let favorites = document.get("favorites") as? [String] else { return }
            
            self.movieStrings.append(contentsOf: favorites)
            
            var i = 0
            
            while i < self.movieStrings.count {
                let contents: String = self.movieStrings[i]
                let contentArray = contents.components(separatedBy: "#")
                
                let genreNum = [88]

                do {
                    let newMovie = try Movie(title: contentArray[0], releaseDate: contentArray[1], plot: contentArray[3], movieKey: 0, genreId: genreNum, poster: contentArray[4], backdrop: contentArray[5])
                    self.movies.append(newMovie)
                    self.favMoviesCollectionView.reloadData()
                    
                } catch {
                    print("Movie not added")
                }
                
                i+=1
            }
            
        }
        
    }
    
    
    @IBAction func backTapped(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "unwindToMain", sender: self)
    }
    

    // MARK: CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MovieCollectionViewCell
        
        cell.movieImage.image = movies[indexPath.row].movieImage
        
        if favMoviePosterImage.image == nil{
            // Display first movie data on startup
            favMoviePosterImage.image = movies[0].movieBackground
            favMovieTitleLabel.text = movies[0].title
            favRuntimeGenreLabel.text = "Genre: \(movies[0].genres)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let newDateFormatter = DateFormatter()
            newDateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = dateFormatter.date(from: movies[0].releaseDate) {
                favReleaseDateLabel.text = "Released: \(newDateFormatter.string(from: date))"
            }
            else {
                favReleaseDateLabel.text = "Released: Unknown"
            }
            favPlotLabel.text = movies[0].plot
            
            // Show all labels
            favMovieTitleLabel.alpha = 1
            favRuntimeGenreLabel.alpha = 1
            favReleaseDateLabel.alpha = 1
            favPlotLabel.alpha = 1
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Showing all labels
        favMovieTitleLabel.textColor = UIColor.white
        favMovieTitleLabel.alpha = 1
        favRuntimeGenreLabel.alpha = 1
        favReleaseDateLabel.alpha = 1
        favPlotLabel.alpha = 1
        //trailerWebView.alpha = 1
        
        // Reset scrollView to top
        //detailScrollView.setContentOffset(CGPoint(x: 0.0,y: 0.0), animated: false)
        
        // Reset movie list to beginning when movie category is changed, this doenst belong here, movie it when needed!!!
        //collectionView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: false)
        
        favMoviePosterImage.image = movies[indexPath.row].movieBackground
        favMovieTitleLabel.text = movies[indexPath.row].title
        favRuntimeGenreLabel.text = "Genre: \(movies[indexPath.row].genres)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "MMM dd, yyyy"
        
        if let date = dateFormatter.date(from: movies[indexPath.row].releaseDate) {
            favReleaseDateLabel.text = "Released: \(newDateFormatter.string(from: date))"
        }
        else {
            favReleaseDateLabel.text = "Release: Unknown"
        }
        
        favPlotLabel.text = movies[indexPath.row].plot
        
        getMovieTrailer(jsonAtUrl: String(movies[indexPath.row].movieKey))
        
        // Set currentMovieId
        currentMovieId = movies[indexPath.row].movieKey    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getMovieTrailer(jsonAtUrl movieId: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let urlStart = "https://api.themoviedb.org/3/movie/"
        let urlEnd = "/videos?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US"
        let completeURL = urlStart + movieId + urlEnd
        
        if let validURL = URL(string: completeURL) {
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail on error
                if opt_error != nil { return }
                
                //Check response, statusCode and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize the data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        //Parse Data
                        guard let sourceData = json["results"] as? [[String: Any]]
                            else { assertionFailure(); return
                        }
                        
                        //print(sourceData)
                        
                        for sources in sourceData {
                            let key = sources["key"] as? String
                            print(key! + " is the currently selected movies trailer key...")
                            self.currentMoviePath = key!
                        }
                        
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
                DispatchQueue.main.async {
                    //self.trailerView.load(withVideoId: self.currentMoviePath, playerVars: ["playsinline": 1])
                    self.trailerView.load(withVideoId: self.currentMoviePath)
                }
                
            })
            task.resume()
        }
    }

}
