//
//  MainViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 10/18/20.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import AVKit
import WebKit
import youtube_ios_player_helper

class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    
    @IBOutlet weak var detailScrollView: UIView!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    @IBOutlet weak var moviePosterImage: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var runtimeGenreLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var plotLabel: UILabel!
    
    @IBOutlet weak var trailerView: YTPlayerView!
    //@IBOutlet weak var trailerWebView: WKWebView!
    
    @IBOutlet weak var movieCategorySegmentedControl: UISegmentedControl!
    
    //var videoPlayer:AVPlayer?
    //var videoPlayerLayer:AVPlayerLayer?
    
    var movies = [Movie]()
    var nowPlayingTemp = [Movie]()
    var popularTemp = [Movie]()
    var upcomingTemp = [Movie]()
    
    var currentMoviePath = ""
    var currentMoviePosition = 0
    var currentMovie = [Movie]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        // Call API requests
        getMovieData(jsonAtUrl: "http://api.themoviedb.org/3/movie/now_playing?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1")
        getMovieData(jsonAtUrl: "http://api.themoviedb.org/3/movie/top_rated?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1")
        getMovieData(jsonAtUrl: "http://api.themoviedb.org/3/movie/upcoming?api_key=5d3e508cd0191203f68265bfcd599e14&language=en-US&page=1")
        
        // Change text color of movieCategorySegmentedControl
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .normal)
    }
    
    // MARK: Actions
    @IBAction func segmentedControlTapped(_ sender: UISegmentedControl) {
        if movieCategorySegmentedControl.selectedSegmentIndex == 0 {
            movies.removeAll()
            movies = nowPlayingTemp
            moviesCollectionView.reloadData()
        }
        else if movieCategorySegmentedControl.selectedSegmentIndex == 1 {
            movies.removeAll()
            movies = popularTemp
            moviesCollectionView.reloadData()
        }
        else if movieCategorySegmentedControl.selectedSegmentIndex == 2 {
            movies.removeAll()
            movies = upcomingTemp
            moviesCollectionView.reloadData()
        }
        
        // Reset movie list to beginning when movie category is changed, this doenst belong here, movie it when needed!!!
        moviesCollectionView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: false)
        
        
    }
    
    @IBAction func profileSettingsButtonTapped(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func favoritesButtonTapped(_ sender: UIBarButtonItem) {
    }
    
    
    @IBAction func searchButtonTapped(_ sender: UIBarButtonItem) {
    }
    
    
    @IBAction func addToFavoritesTapped(_ sender: UIButton) {
        
        let userId = Auth.auth().currentUser?.uid
        let db = Firestore.firestore()
        
        // Add the currentMovieId to the users favorites list
        
        let movieToAdd = currentMovie[0].title + "#" + "Released: " + currentMovie[0].releaseDate + "#" + "Genre: " + currentMovie[0].genres
            + "#" + currentMovie[0].plot + "#" + currentMovie[0].poster + "#" + currentMovie[0].backdrop
        
        
        let docRef = db.collection("users").document(userId!)
        
        docRef.updateData([
            "favorites": FieldValue.arrayUnion([movieToAdd])
        ])
        
    }
    
    // MARK: CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MovieCollectionViewCell
        
        cell.movieImage.image = movies[indexPath.row].movieImage
        
        if moviePosterImage.image == nil{
            // Display first movie data on startup
            moviePosterImage.image = movies[0].movieBackground
            movieTitleLabel.text = movies[0].title
            runtimeGenreLabel.text = "Genre: \(movies[0].genres)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let newDateFormatter = DateFormatter()
            newDateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = dateFormatter.date(from: movies[0].releaseDate) {
                releaseDateLabel.text = "Released: \(newDateFormatter.string(from: date))"
            }
            else {
                releaseDateLabel.text = "Released: Unknown"
            }
            plotLabel.text = movies[0].plot
            
            getMovieTrailer(jsonAtUrl: String(movies[0].movieKey))
            
            // Show all labels
            movieTitleLabel.alpha = 1
            runtimeGenreLabel.alpha = 1
            releaseDateLabel.alpha = 1
            plotLabel.alpha = 1
            
            currentMovie.append(movies[0])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        currentMovie.removeAll()
        
        // Showing all labels
        movieTitleLabel.textColor = UIColor.white
        movieTitleLabel.alpha = 1
        runtimeGenreLabel.alpha = 1
        releaseDateLabel.alpha = 1
        plotLabel.alpha = 1
        //trailerWebView.alpha = 1
        
        // Reset scrollView to top
        //detailScrollView.setContentOffset(CGPoint(x: 0.0,y: 0.0), animated: false)
        
        moviePosterImage.image = movies[indexPath.row].movieBackground
        movieTitleLabel.text = movies[indexPath.row].title
        runtimeGenreLabel.text = "Genre: \(movies[indexPath.row].genres)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "MMM dd, yyyy"
        
        if let date = dateFormatter.date(from: movies[indexPath.row].releaseDate) {
            releaseDateLabel.text = "Released: \(newDateFormatter.string(from: date))"
        }
        else {
            releaseDateLabel.text = "Release: Unknown"
        }
        
        plotLabel.text = movies[indexPath.row].plot
        
        getMovieTrailer(jsonAtUrl: String(movies[indexPath.row].movieKey))
        
        currentMovie.append(movies[indexPath.row])
    }
    
    // MARK: - Navigation
    func transitionToLoginVC() {
        let loginViewController = storyboard?.instantiateViewController(identifier: "LoginVC") as? ViewController
        view.window?.rootViewController = loginViewController
        view.window?.makeKeyAndVisible()
    }
    
    func transitionToSearchVC() {
        let searchViewController = storyboard?.instantiateViewController(identifier: "SearchVC") as? ViewController
        view.window?.rootViewController = searchViewController
        view.window?.makeKeyAndVisible()
    }
    
    func transitionToSettingsVC() {
        let searchViewController = storyboard?.instantiateViewController(identifier: "SettingsVC") as? ViewController
        view.window?.rootViewController = searchViewController
        view.window?.makeKeyAndVisible()
    }
    
    @IBAction func unwindToMain(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }

//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
    

}
