//
//  PwRecoverViewController.swift
//  MyFlix
//
//  Created by Tony Puente on 10/18/20.
//

import UIKit
import FirebaseAuth

class PwRecoverViewController: UIViewController {

    @IBOutlet weak var recoverEmailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    @IBAction func submitTapped(_ sender: UIButton) {
        
        let userEmail = recoverEmailTextField.text
        
        Auth.auth().sendPasswordReset(withEmail: userEmail!) { (error) in
            if error != nil {
                // There was an error sending a recovery email
                print(error!.localizedDescription)
            } else {
                print("Recovery Successful...Check email")
                self.transitionToLogin()
            }
        }
        
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        
        // Set up Unwind feature
        self.performSegue(withIdentifier: "unwindToLogin", sender: self)
    }
    
    
    // MARK: - Navigation
     
    // Transition to main view controller
    func transitionToLogin () {
        let loginViewController = storyboard?.instantiateViewController(withIdentifier: "loginVC") as? LoginViewController
        view.window?.rootViewController = loginViewController
        view.window?.makeKeyAndVisible()
    }
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}
