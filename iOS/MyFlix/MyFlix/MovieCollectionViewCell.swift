//
//  MovieCollectionViewCell.swift
//  MyFlix
//
//  Created by Tony Puente on 10/27/20.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    
}
