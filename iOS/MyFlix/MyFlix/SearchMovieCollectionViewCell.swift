//
//  SearchMovieCollectionViewCell.swift
//  MyFlix
//
//  Created by Tony Puente on 11/16/20.
//

import UIKit

class SearchMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var searchMovieImage: UIImageView!
    
}
