//
//  Movie.swift
//  MyFlix
//
//  Created by Tony Puente on 10/27/20.
//

import Foundation
import UIKit

class Movie {
    
    //Stored Properties
    let title: String
    let releaseDate: String
    let plot: String
    let movieKey: Int
    let genreId: [Int]
    let poster: String
    let backdrop: String
    var movieImage: UIImage! = nil
    var movieBackground: UIImage! = nil
    
    //Computed Properties
    var genres: String {
        let firstGenre = genreId[0]
        
        var genreName = ""
        
        switch firstGenre {
        case 28:
            genreName = "Action"
        case 12:
            genreName = "Adventure"
        case 16:
            genreName = "Animation"
        case 35:
            genreName = "Comedy"
        case 80:
            genreName = "Crime"
        case 99:
            genreName = "Documentary"
        case 18:
            genreName = "Drama"
        case 10751:
            genreName = "Family"
        case 14:
            genreName = "Fantasy"
        case 36:
            genreName = "History"
        case 27:
            genreName = "Horror"
        case 10402:
            genreName = "Music"
        case 9648:
            genreName = "Mystery"
        case 10749:
            genreName = "Romance"
        case 878:
            genreName = "Science Fiction"
        case 10770:
            genreName = "TV Movie"
        case 53:
            genreName = "Thriller"
        case 10752:
            genreName = "War"
        case 37:
            genreName = "Western"
        default:
            genreName = "Some other genre"
        }
        
        return genreName
    }
    
    //Initializer
    init(title: String, releaseDate: String, plot: String, movieKey: Int, genreId: [Int], poster: String, backdrop: String) throws {
        self.title = title
        self.releaseDate = releaseDate
        self.plot = plot
        self.movieKey = movieKey
        self.genreId = genreId
        self.poster = poster
        self.backdrop = backdrop
        
        //Get Movie Poster from TMDB
        if let url = URL(string: "https://image.tmdb.org/t/p/w500\(poster)") {
            let imgData = try Data.init(contentsOf: url)
            self.movieImage = UIImage(data: imgData)
            //print("\(title) saved")
        }
        else {
            self.movieImage = UIImage.init(named: "NoImage")
        }
        
        //Get Movie Backdrop
        if let url2 = URL(string: "https://image.tmdb.org/t/p/w500\(backdrop)") {
            let imgData2 = try Data.init(contentsOf: url2)
            self.movieBackground = UIImage(data: imgData2)
        }
        else {
            self.movieBackground = UIImage.init(named: "NoImage")
        }
    }
}
